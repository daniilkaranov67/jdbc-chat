package dev.company.db.mapper.impl;

import dev.company.db.entity.Message;
import dev.company.db.entity.MessageDeleteStatus;
import dev.company.db.mapper.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageRowMapper implements RowMapper<Message> {
    @Override
    public Message mapRow(ResultSet resultSet) throws SQLException {
        return new Message(
                resultSet.getInt("id"),
                resultSet.getString("text"),
                resultSet.getInt("chat_id"),
                MessageDeleteStatus.valueOf(resultSet.getString("delete_status")),
                resultSet.getBoolean("is_read"),
                resultSet.getInt("author_id")
        );
    }
}
