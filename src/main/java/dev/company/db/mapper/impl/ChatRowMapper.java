package dev.company.db.mapper.impl;

import dev.company.db.entity.Chat;
import dev.company.db.mapper.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ChatRowMapper implements RowMapper<Chat> {
    @Override
    public Chat mapRow(ResultSet resultSet) throws SQLException {
        return new Chat(
                resultSet.getInt("id"),
                resultSet.getInt("author_id"),
                resultSet.getString("topic")
        );
    }
}
