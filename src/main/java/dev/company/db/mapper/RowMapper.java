package dev.company.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<Entity> {
    Entity mapRow(ResultSet resultSet) throws SQLException;
}
