package dev.company.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionSource {
    private static final String username = "postgres";
    private static final String password = "123";
    private static final String url = "jdbc:postgresql://localhost:5432/test";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
}
