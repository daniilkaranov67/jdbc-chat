package dev.company.db.dto;

import dev.company.db.entity.MessageDeleteStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto {
    private Integer id;
    private String text;
    private Integer chatId;
    private MessageDeleteStatus status;
    private boolean isRead;
    private Integer authorId;
}
