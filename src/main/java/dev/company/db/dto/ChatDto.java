package dev.company.db.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatDto {
    private Integer id;
    private Integer authorId;
    private String topicName;
    private List<MessageDto> messages;
}
