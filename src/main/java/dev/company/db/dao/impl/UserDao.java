package dev.company.db.dao.impl;

import dev.company.db.ConnectionSource;
import dev.company.db.dao.DaoCRUD;
import dev.company.db.entity.Chat;
import dev.company.db.entity.User;
import dev.company.db.mapper.RowMapper;
import dev.company.db.mapper.impl.UserRowMapper;
import dev.company.db.util.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements DaoCRUD<User, Integer> {

    private final RowMapper<User> rowMapper = new UserRowMapper();
    private final Connection connection;

    public UserDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery("SELECT * FROM users;")) {
                while (resultSet.next()) {
                    users.add(rowMapper.mapRow(resultSet));
                }
            }
        } catch (SQLException e) {
            Logger.error("error during find all", e);
        }
        return users;
    }

    @Override
    public User findById(Integer id) {
        User user = null;
        try (PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT * FROM users WHERE id=?")) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    user = rowMapper.mapRow(resultSet);
                }
            }
        } catch (SQLException e) {
            Logger.error("error during find all", e);
        }
        return user;
    }

    @Override
    public int create(User user) {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO users (username, password) VALUES (?, ?);",
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Affected row is 0.");
            }
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                Logger.info("Generated id for User: " + resultSet.getInt(1));
                return resultSet.getInt(1);
            }
            statement.execute();
        } catch (SQLException e) {
            Logger.error("error during create user", e);
        }
        return -1;
    }
}
