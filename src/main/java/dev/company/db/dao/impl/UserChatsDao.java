package dev.company.db.dao.impl;

import dev.company.db.entity.UserChats;
import dev.company.db.util.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserChatsDao {

    private final Connection connection;

    public UserChatsDao(Connection connection) {
        this.connection = connection;
    }

    public void create(UserChats userChats) {
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO user_chats (user_id, chat_id) VALUES (?, ?);")) {
            statement.setInt(1, userChats.getUserId());
            statement.setInt(2, userChats.getChatId());
            statement.execute();
        } catch (SQLException e) {
            Logger.error("error during create user chats", e);
        }
    }
}
