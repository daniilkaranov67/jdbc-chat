package dev.company.db.dao.impl;

import dev.company.db.dao.DaoCRUD;
import dev.company.db.entity.Chat;
import dev.company.db.mapper.RowMapper;
import dev.company.db.mapper.impl.ChatRowMapper;
import dev.company.db.util.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ChatDao implements DaoCRUD<Chat, Integer> {

    private final RowMapper<Chat> mapper = new ChatRowMapper();
    private final Connection connection;

    public ChatDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Chat findById(Integer id) {
        Chat chat = null;
        try (PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT * FROM chat WHERE id=?")) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    chat = mapper.mapRow(resultSet);
                }
            }
        } catch (SQLException e) {
            Logger.error("error during find all", e);
        }
        return chat;
    }

    @Override
    public int create(Chat chat) {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO chat (author_id, topic) VALUES (?, ?);",
                Statement.RETURN_GENERATED_KEYS
        )) {
            statement.setInt(1, chat.getAuthorId());
            statement.setString(2, chat.getTopicName());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Affected row is 0.");
            }
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                Logger.info("Generated id for Chat: " + resultSet.getInt(1));
                return resultSet.getInt(1);
            } else {
                throw new SQLException("No id obtained.");
            }
        } catch (SQLException e) {
            Logger.error("error during create chat", e);
        }
        return -1;
    }

}
