package dev.company.db.dao.impl;

import dev.company.db.dao.DaoCRUD;
import dev.company.db.entity.Message;
import dev.company.db.mapper.RowMapper;
import dev.company.db.mapper.impl.MessageRowMapper;
import dev.company.db.util.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageDao implements DaoCRUD<Message, Integer> {

    private final RowMapper<Message> rowMapper = new MessageRowMapper();
    private final Connection connection;

    public MessageDao(Connection connection) {
        this.connection = connection;
    }

    public List<Message> findAllMessageByChatId(Integer chatId) {
        List<Message> messages = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM message WHERE chat_id=?")) {
            preparedStatement.setInt(1, chatId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    messages.add(rowMapper.mapRow(resultSet));
                }
            }
        } catch (SQLException e) {
            Logger.error("error during find all", e);
        }
        return messages;
    }
}
