package dev.company.db.dao;

import java.util.List;

public interface DaoCRUD<Entity, Id> {
    default List<Entity> findAll() {
        throw new UnsupportedOperationException();
    }
    default Entity findById(Integer id) {
        throw new UnsupportedOperationException();
    }
    default void deleteById(Integer id) {
        throw new UnsupportedOperationException();
    }
    default void update(Entity entity) {
        throw new UnsupportedOperationException();
    }
    default int create(Entity entity) {
        throw new UnsupportedOperationException();
    }
}
