package dev.company.db.service;

import dev.company.db.dao.impl.UserDao;
import dev.company.db.dto.CreateUserDto;
import dev.company.db.dto.UserDto;
import dev.company.db.entity.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {
    private final UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public List<UserDto> findAll() {
        return userDao.findAll().stream().map(this::convertUserToUserDto).collect(Collectors.toList());
    }

    public void create(CreateUserDto userDto) {
        userDao.create(new User(null, userDto.getUsername(), userDto.getPassword()));
    }

    private UserDto convertUserToUserDto(User user) {
        return new UserDto(
                user.getId(),
                user.getUsername()
        );
    }
}
