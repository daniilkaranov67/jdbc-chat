package dev.company.db.service;

import dev.company.db.dao.impl.ChatDao;
import dev.company.db.dao.impl.MessageDao;
import dev.company.db.dao.impl.UserChatsDao;
import dev.company.db.dto.ChatDto;
import dev.company.db.dto.MessageDto;
import dev.company.db.entity.Chat;
import dev.company.db.entity.Message;
import dev.company.db.entity.UserChats;

import java.util.List;
import java.util.stream.Collectors;

public class ChatService {
    private final ChatDao chatDao;
    private final MessageDao messageDao;
    private final UserChatsDao userChatsDao;

    public ChatService(ChatDao chatDao, MessageDao messageDao, UserChatsDao userChatsDao) {
        this.chatDao = chatDao;
        this.messageDao = messageDao;
        this.userChatsDao = userChatsDao;
    }

    public ChatDto findById(Integer id) {
        Chat chat = chatDao.findById(id);
        return mapChatToChatDto(
                chat,
                messageDao.findAllMessageByChatId(id)
                        .stream().map(this::mapMessageToMessageDto).collect(Collectors.toList())
        );
    }

    public void createChat(ChatDto chatDto) {
        Chat chat = mapChatDtoToChat(chatDto);
        int chatId = chatDao.create(chat);
        userChatsDao.create(new UserChats(chatId, chat.getAuthorId()));
    }

    private Chat mapChatDtoToChat(ChatDto dto) {
        return new Chat(
                dto.getId(),
                dto.getAuthorId(),
                dto.getTopicName()
        );
    }

    private ChatDto mapChatToChatDto(Chat chat, List<MessageDto> messages) {
        return new ChatDto(
                chat.getId(),
                chat.getAuthorId(),
                chat.getTopicName(),
                messages
        );
    }

    private MessageDto mapMessageToMessageDto(Message message) {
        return new MessageDto(
                message.getId(),
                message.getText(),
                message.getChatId(),
                message.getStatus(),
                message.isRead(),
                message.getAuthorId()
        );
    }
}
