package dev.company.db.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private Integer id;
    private String text;
    private Integer chatId;
    private MessageDeleteStatus status;
    private boolean isRead;
    private Integer authorId;
}
