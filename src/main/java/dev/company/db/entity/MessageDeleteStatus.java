package dev.company.db.entity;

public enum MessageDeleteStatus {
    NOT_DELETED,
    DELETED_FOR_AUTHOR,
    DELETED_FOR_ALL
}
