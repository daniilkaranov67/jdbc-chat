package dev.company;

import dev.company.db.ConnectionSource;
import dev.company.db.dao.impl.ChatDao;
import dev.company.db.dao.impl.MessageDao;
import dev.company.db.dao.impl.UserChatsDao;
import dev.company.db.dao.impl.UserDao;
import dev.company.db.dto.ChatDto;
import dev.company.db.service.ChatService;
import dev.company.db.service.UserService;
import dev.company.db.util.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class Application {
    public static void main(String[] args) {
        try (Connection connection = ConnectionSource.getConnection()) {
            UserDao userDao = new UserDao(connection);
            ChatDao chatDao = new ChatDao(connection);
            MessageDao messageDao = new MessageDao(connection);
            UserChatsDao userChatsDao = new UserChatsDao(connection);

            UserService userService = new UserService(userDao);
            ChatService chatService = new ChatService(chatDao, messageDao, userChatsDao);

            chatService.createChat(new ChatDto(null, 1, "Cooking", null));
            System.out.println(userService.findAll());
            System.out.println(chatService.findById(1));
        } catch (SQLException e) {
            Logger.error("error during get connection", e);
        }
    }
}
